#Traktor V2 Beta (UnStable)
#if you need Stable Version, go to [this](https://gitlab.com/TraktorPlus/Traktor/tree/V1.9) page

# Traktor V2 Beta
Traktor will autamically install Tor, privoxy, dnscrypt-proxy and Tor Browser Launcher in either a Debian based distro like Ubuntu, an Arch based distro, Fedora based distro or an OpenSUSE based distro and configures them as well.

To do this, just run 'installer.sh' file in a supported shell like bash and watch for prompts it asks you.

## Note
Do NOT expect anonymity using this method. Privoxy is an http proxy and can leak data. If you need anonymity or strong privacy, manually run torbrowser-launcher after installing traktor and use it.

## Manual Pages
[راهنمای اسکریپت تراکتور به زبان فارسی](https://soshaw.net/traktor/) {lang=FA}

## Install
### Ubuntu and other Distro
    wget https://gitlab.com/TraktorPlus/Traktor/repository/V2-Beta/archive.zip -O traktor-V2-beta.zip
    unzip traktor-V2-beta.zip -d ~/Traktor-V2-Beta && cd Traktor-V2-Beta/*
    chmod +x installer.sh
    ./installer.sh
### ArchLinux
    yaourt -S traktor
### Other (May not be able to install yet)
    sudo apt install python-gi #(Optional, for having a graphical indicator)
    sudo apt install gir1.2-appindicator3-0.1 #(Optional, for having a graphical indicator)

## Changes
[See Changes](https://gitlab.com/TraktorPlus/Traktor/blob/V2-Beta/CHANGELOG)
