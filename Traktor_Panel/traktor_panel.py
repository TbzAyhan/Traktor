#!/usr/bin/python

#########################
#     Traktor V2.0      #
#   System Tray Icon    #
#  Version: 1.0 (beta)  #
#########################

import os
import signal
import gi

gi.require_version('Gtk','3.0')
gi.require_version('AppIndicator3','0.1')
gi.require_version('Notify','0.7')

from gi.repository import Gtk, Gio, Notify
from gi.repository import AppIndicator3 as appindicator
from gettext import gettext as T
from time import sleep
from os import popen #TODO remove dependency

APPINDICATOR_ID = 'traktor'
Name = T("Traktor")

#Commands #TODO remove bash commands
RESTART_TOR = 'sudo systemctl restart tor.service'

#Current Working Directory #TODO Move to /usr/share/traktor/icons
cwd = os.path.abspath(os.path.dirname(__file__))

#Icon pathes
ENABLE_PROXY = os.environ.get("HOME") + "/.Traktor/Traktor_Panel/icons/enable_proxy.svg"
DISABLE_PROXY = os.environ.get("HOME") + "/.Traktor/Traktor_Panel/icons/disable_proxy.svg"

proxy = Gio.Settings.new("org.gnome.system.proxy")
if (Gio.Settings.get_string(proxy, "mode")=="manual"):
    indicator = appindicator.Indicator.new(APPINDICATOR_ID,ENABLE_PROXY,appindicator.IndicatorCategory.SYSTEM_SERVICES)
else:
    indicator = appindicator.Indicator.new(APPINDICATOR_ID,DISABLE_PROXY,appindicator.IndicatorCategory.SYSTEM_SERVICES)

def main(indicator):
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
    indicator.set_menu(build_menu())
    Notify.init(APPINDICATOR_ID)
    Gtk.main() 

def build_menu():
    menu = Gtk.Menu()
    
    img_proxy = Gtk.Image()
    img_proxy.set_from_file(os.environ.get("HOME") + "/.Traktor/Traktor_Panel/icons/enable_proxy.svg")
    item_proxy = Gtk.ImageMenuItem('Enable Proxy')
    item_proxy.set_image(img_proxy)
    item_proxy.set_always_show_image(True)
    item_proxy.connect('activate', enable_proxy)
    
    img_normal = Gtk.Image()
    img_normal.set_from_file(os.environ.get("HOME") + "/.Traktor/Traktor_Panel/icons/disable_proxy.svg")
    item_normal = Gtk.ImageMenuItem('Disable Proxy')
    item_normal.set_image(img_normal)
    item_normal.set_always_show_image(True)
    item_normal.connect('activate', disable_proxy)

    img_torrestart = Gtk.Image()
    img_torrestart.set_from_file(os.environ.get("HOME") + "/.Traktor/Traktor_Panel/icons/tor-service_restart.svg")
    item_torrestart = Gtk.ImageMenuItem('Restart Tor')
    item_torrestart.set_image(img_torrestart)
    item_torrestart.set_always_show_image(True)
    item_torrestart.connect('activate', restart)
    
    sep = Gtk.SeparatorMenuItem()
    
    img_quit = Gtk.Image()
    img_quit.set_from_file()
    item_quit = Gtk.ImageMenuItem('Quit')
    item_quit.set_image(img_quit)
    item_quit.set_always_show_image(True)
    item_quit.connect('activate', quit)
    
    menu.append(item_proxy)
    menu.append(item_normal)
    menu.append(item_torrestart)
    #menu.append(sep)
    menu.append(item_quit)
    
    menu.show_all()
    
    return menu 

def disable_proxy(_):
    proxy.set_string("mode", "none")
    indicator.set_icon(str(os.path.abspath(DISABLE_PROXY)))
    Notify.Notification.new(Name, T("Proxy is Disabled"), None).show()

def enable_proxy(_):
    proxy.set_string("mode", "manual")
    indicator.set_icon(str(os.path.abspath(ENABLE_PROXY)))
    Notify.Notification.new(Name, T("Proxy is Enabaled"), None).show()

def restart(_):
	try:
		restart_tor_service = subprocess.check.output(RESTART_TOR,stderr=subprocess.STDOUT,shell=True)
		sleep(3)
		if restart_tor_service != "":
			Notify.Notification.new(Name, T("Restarting Failed! See the Tor log..."), None).show()
		else:
			Notify.Notification.new(Name, T("Tor Restarted"), None).show()
	except Exception:
		Notify.Notification.new(Name, T("Restarting Failed! See the Tor log..."), None).show()

def quit(_):
    Notify.uninit()
    Gtk.main_quit()

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)

main(indicator)
