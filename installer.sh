#!/bin/bash


##########################################
#               Traktor V2               #
# https://gitlab.com/TraktorPlus/Traktor #
##########################################


clear


# =========\Create File and Folder\=========


if [ ! -d "$HOME/.Traktor/" ]; then
    mkdir "$HOME/.Traktor"
fi
if [ ! -d "$HOME/.Traktor/Traktor_Log/" ]; then
    mkdir "$HOME/.Traktor/Traktor_Log"
fi
if [ ! -e "$HOME/.Traktor/Traktor_Log/traktor.log" ]; then
    echo -e "Traktor Log: [$(date)]" | tee $HOME/.Traktor/Traktor_Log/traktor.log > /dev/null
fi
if [ ! -e "$HOME/.Traktor/Traktor_Log/traktor_status.log" ]; then
    echo -e "Traktor Status Log: [$(date)]" | tee $HOME/.Traktor/Traktor_Log/traktor_status.log > /dev/null
fi

# =========/Create File and Folder/=========


# =========\Main\=========
check_distro=0
#Checking if the distro is debianbase / archbase / redhatbase/ openSUSEbae and running the correct script
if pacman -Q &> /dev/null ;then # Check Arch
    if [ ! -f ./traktor_arch.sh ]; then
        wget -O ./traktor_arch.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_arch.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_arch.sh
    fi
    sudo chmod +x ./traktor_arch.sh
    ./traktor_arch.sh # Run Traktor Arch
elif apt list --installed &> /dev/null ;then # Check Debian
    if [ ! -f ./traktor_debian.sh ]; then
        wget -O ./traktor_debian.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_debian.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_debian.sh
    fi
    sudo chmod +x ./traktor_debian.sh 
    ./traktor_debian.sh # Run Traktor Debian
elif dnf list &> /dev/null ;then
    if [ ! -f ./traktor_fedora.sh ]; then # Check RedHat
        wget -O ./traktor_fedora.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_fedora.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_fedora.sh
    fi
    sudo chmod +x ./traktor_fedora.sh
    ./traktor_fedora.sh # Run Traktor Fedora
elif zypper search i+ &> /dev/null ;then
    if [ ! -f ./traktor_opensuse.sh ]; then # Check OpenSUSE
        wget -O ./traktor_opensuse.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_opensuse.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/V2-Beta/traktor_opensuse.sh
    fi
    sudo chmod +x ./traktor_opensuse.sh
    ./traktor_opensuse.sh # Run Traktor OpenSUSE
else
    echo "Your distro is neither archbase nor debianbase nor redhatbase nor susebase So, The script is not going to work in your distro."
    check_distro="1"
fi


echo -e "\n[$(date)] traktor installed " | tee -a ~/.Traktor/Traktor_Log/traktor.log > /dev/null
if [ "$check_distro" == "0" ];then
#==========Adding Traktor Command================
	sudo cp traktor /usr/bin/traktor 
	command_adding="$?"
	sudo chmod 755 /usr/bin/traktor
	change_mode="$?"
	if [ "$command_adding" == "0" ] && [ "$change_mode" == "0" ] ; then
	    echo "Now Use this command 'traktor --help'"
	fi
#=======Traktor Command Must Be Added============
else
	exit 1
fi
